<?php
    //Константы для обращения к контроллерам
        define('PathPrefixController','/controllers/');
        define('PathPostfixController','Controller');
    //Константы для обращения к Моделям
        define('PathPrefixModel','/models/');
        define('PathPostfixModel','Model.php');
        //Константы для обращения к Методам
        define('PathPostfixAction','Action');
                    //Шаблон Smarty
                    $template = 'default';
//Пути к файлам шаблонов (*.tpl)
define('TemplatePrefix',S_ROOT.'/views/'.$template.'/');
define('TemplatePostfix',".tpl");

//Путь к файлом шаблонов в вебпроснстрансве
define('TemplateWebPath','/templates/'.$template.'/');
