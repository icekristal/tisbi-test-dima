
/*Приемка формы*/
function SendTestingForm(form) {
    var $form = $(form),
        $formId = $form.attr('id');
    var data_form = $("#" + $formId).serialize();
    var message_field = $('.b-field-test__message');
    message_field.removeClass("success");
    message_field.removeClass("error");
    $.ajax({
        url: '/dispatch/sendtesting/',
        type: 'post',
        data: data_form,
        dataType: "json",
        beforesend : function () {
            message_field.html("Отправка...");
        },
        success: function(data) {
            message_field.html(data['message']);
            if(data['success']){
                message_field.addClass("success");
            }else{
                message_field.addClass("error");
            }
        },
        error:function(err){
            alert(err);
        }
    });
}