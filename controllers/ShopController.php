<?php

class ShopController
{

    /*
     *  Я так понимаю нужны просто процедуры, так что БД подключать не надо думаю, предположу что
     * $db - это подключение к базу (PDO)
     * Так же не образай внимание что все в контролере, это просто пример=) На реальном примере делал бы в моделях
     * */


    public static function makeNewOrder($user_id,$comment,$array_item){
        $db = "Подключили бд, так не много не правильно, но думаю для примера норм=))";
        $rs = $db->prepare("
            INSERT INTO `orders` (`user_id`,`comment`,`date_created`)
            VALUES (:user_id,:comment,NOW())
        ");
        $rs->bindParam(":user_id",$user_id, PDO::PARAM_INT);
        $rs->bindParam(":comment",$comment, PDO::PARAM_STR);
        $rs->execute();
        $order_id = $db->lastInsertId();
        self::makeInfoOrder($order_id,$array_item);
    }
        private static function makeInfoOrder($order_id,$array_items_info){
        // $array_items_info - то массив продуктов, предположительно взятый из корзины с данными о его цене и количестве
            $db = "";
//            $fields=array('order_id','item_id','price_item','count_item');
//
//            $db->beginTransaction();
//            $insert_values = array();
//            foreach($array_items_info as $d){
//                $question_marks[] = '('  . placeholders('?', sizeof($d)) . ')';
//                $insert_values = array_merge($insert_values, array_values($d));
//            }
//
//            $sql = "INSERT INTO `order_info` (" . implode(",", $fields ) . ") VALUES " .implode(',', $question_marks);
//
//            $stmt = $db->prepare ($sql);
//            try {
//                $stmt->execute($insert_values);
//            } catch (PDOException $e){
//                echo $e->getMessage();
//            }
//            $db->commit();

            $sql = "INSERT INTO `order_info`
    (`order_id`,`item_id`,`price_item`,`count_item`)
    VALUES ";

            $values = array();
            foreach ($array_items_info as $item){
                $values[] = "('{$order_id}','{$item['id']}','{$item['price']}','{$item['cart_in_count']}')";
            }
            $sql .= implode($values,', ');
           $rs = $db->query($sql);
            return $rs;
    }


    public static function makeNewUser($login,$password,$name,$s_name,$email){

        $db = "";

        /*
         * Так еще по хорошему, проверка вообще есть ли такой пользователь, кодирование пароля
         * проверка почту он ввел или нет
         */

        $rs = $db->prepare("
            INSERT INTO `users` (`login`,`password`,`name`,`s_name`,`email`,`date_created`)
            VALUES (:login,:password,:name,:s_name,:email,NOW())
        ");
        $rs->bindParam(":login",$login, PDO::PARAM_STR);
        $rs->bindParam(":password",$password, PDO::PARAM_STR);
        $rs->bindParam(":name",$name, PDO::PARAM_STR);
        $rs->bindParam(":s_name",$s_name, PDO::PARAM_STR);
        $rs->bindParam(":email",$email, PDO::PARAM_STR);
        $rs->execute();
    }



    /*Давно не писал так sql запросы=) Обычно это делает миграция=)
        Тут просто переменные с запросами, вроде так было по заданию
    */

    private $sql_create_users_table = "
    
        CREATE TABLE `users` (
      `id` int(11) AUTO_INCREMENT,
      `login` varchar(255) NOT NULL,
      `password` varchar(255) NOT NULL,
      `name` varchar(255) NOT NULL,
      `s_name` varchar(255) NOT NULL,
      `email` varchar(255) NOT NULL,
      `date_created` datetime DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;    
    
      
    ";

    private $sql_create_items_table = "
    
    CREATE TABLE `items` (
  `id` int(11) AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,  
  `image` varchar(255) NOT NULL,
  `price` DOUBLE(10,2) NOT NULL ,
  `date_created` datetime DEFAULT NULL,
  `visible` smallint(1) NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        
    ";

    private $sql_create_orders_table = "
    
    CREATE TABLE `orders` (
  `id` int(11) AUTO_INCREMENT,
  `comment` longtext,
  `user_id` int(11),
  `date_created` datetime DEFAULT NULL,
  `pay` smallint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  
  ALTER TABLE `orders`
  ADD CONSTRAINT `fx-orders-users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

    ";


    /*Всетакие создам доп таблицу, обычно так делаю да и правильнее так наверно, если в корзине более 1 товара*/

    private $sql_create_order_info_table="
    
        CREATE TABLE `order_info` (
  `order_id` int(11),
  `item_id` int(11),
  `price_item` DOUBLE(10,2) NOT NULL ,
  `count_item` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  
  CREATE INDEX `idx-order_info-order_id` ON order_info(order_id);
  CREATE INDEX `idx-order_info-item_id` ON order_info(item_id);
  
  ALTER TABLE `order_info`
  ADD CONSTRAINT `fx-order_info-items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fx-order_info-order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE SET NULL;  
    
    ";



}