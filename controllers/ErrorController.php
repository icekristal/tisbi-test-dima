<?php

use \components\SetSmart as BaseSmart;
use components\ControllerInf;
use models\CityModel;

class ErrorController extends BaseSmart implements ControllerInf
{

    public function IndexAction()
    {
        $all_city_action = CityModel::getAllCity(1,1);
        $this->assign("all_city_action_z",$all_city_action);

        $this->assign("TitleTwo","Ошибка страница не найдена");
        header('HTTP/1.0 404 Not Found');


        $this->LoadSmarty("header");
        $this->LoadSmarty("error");
        $this->LoadSmarty("footer");
        return true;
    }
}