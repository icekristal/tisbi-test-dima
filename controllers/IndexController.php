<?php



use components\ControllerInf;
use components\Settings;
use components\SettingsSmart;
use components\Validator;


class IndexController extends SettingsSmart implements ControllerInf
{

    public function IndexAction()
    {
        Settings::getInstance()->setNewSetting("four_test","Привет четвертая настройка");
        Settings::getInstance()->updateSettingProject("two_test","Изменение 2 настройка");
        Settings::getInstance()->delSettingProject("three_test");

        $this->assign('array_setting', Settings::getInstance()->getSettingsProject());


        $this->LoadSmarty("header");
        $this->LoadSmarty("index");
        $this->LoadSmarty("footer");
        return true;
    }

    public function FormprocessingAction(){
        if(!Validator::isJson()){
            exit();
        }
        $Array_Data = array();  // Массив выходных параметров
        $Array_Data['success']=1;

        /*Первое поле*/
            $field_one = $_POST['field_one'];
            if(preg_match( "/[^a-zа-яё0-9 ]/iu",$field_one) == 1){
                $Array_Data['success']=0;
                $Array_Data['message']="Ошибка: поле 1 (можно вводить только цифры,кирилицу, латиницу и пробел).";
            }
            if (mb_strlen($field_one)>128){
                $Array_Data['success']=0;
                $Array_Data['message']="Ошибка: поле 1 (максимальная длина строки 128 символов).";
            }

        /*Второе поле*/
            $field_two = $_POST['field_two'];
            if(preg_match('/^(?:\d\,)+$/',$field_two)){
                $Array_Data['success']=0;
                $Array_Data['message']="Ошибка: поле 2 (можно вводить только цифры с запятой).";
            }

        /*Третье поле*/
            $field_three = $_POST['field_three'];
        if(preg_match( "/[^а-яё ]/iu",$field_three) == 1){
            $Array_Data['success']=0;
            $Array_Data['message']="Ошибка: поле 3 (можно вводить только кирилицу и пробел).";
        }


        if(!Validator::isEmpty([$field_one,$field_two,$field_three])){
            $Array_Data['success']=0;
            $Array_Data['message']="Все поля должны быть заполнены";
        }

            if ($Array_Data['success']){

                $new_field_two = $field_two;
                if(strpos($field_two,",")){

                $ar_field_two = explode(",",$field_two);
                    if(isset($ar_field_two)){
                        $new_field_two = $ar_field_two[0].",".substr($ar_field_two[1],0,2);
                    }
                }

                $html_field_three = $field_three;
                if(strpos($field_three," ")){
                    $field_three=trim($field_three);
                    $field_three = preg_replace("/(\s){2,}/",' ',$field_three);
                    $array_field_three = explode(" ",$field_three);
                    $html_field_three="<ul>";
                    foreach ($array_field_three as $word){
                        $html_field_three.="<li>{$word}</li>";
                    }
                    $html_field_three.="</ul>";
                }

                $Array_Data['message']="Все верно. <br>
                <b>Данные поля 1:</b> {$field_one} <br>
                <b>Данные поля 2:</b> {$new_field_two} <br>
                <b>Данные поля 3:</b> {$html_field_three}
                ";
            }


      echo json_encode($Array_Data);
    }

}