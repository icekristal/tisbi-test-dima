    /*Подключаем сам галп*/
    const gulp = require('gulp');
    const sass = require('gulp-sass');
    const browserSync = require('browser-sync');
    const concat = require('gulp-concat');
    //сливаем в 1
    const uglify = require('gulp-uglifyjs');
    //сжимаем js
    const cssnano = require('gulp-cssnano');
    //сжимаем css
    const rename = require('gulp-rename');
    const del = require('del');
    const imagemin = require('gulp-imagemin');
    //для сжатия изображний
    const pngquant = require('imagemin-pngquant');
    // выше сжатие, надо прочитать
    const cache = require('gulp-cache');
    //кеш проекта
    const autoprefixer = require('gulp-autoprefixer');
    //для кроссбраузерности
    const cssMin = require('gulp-css');
    const minimist = require('minimist');

    const pattern = "default/";

const dir_web_prod = "../web/";
const dir_view = "../views/"+pattern;
const dir_dev_lib = "app/libs";
const dir_scss = "app/scss/**/*.scss";
const dir_css = "app/css/*.css";
const dir_js = "app/js/*.js";
const dir_fonts = "app/fonts/**/*.*";
const dir_img = "app/img/**/*";


/*Преобразовываем scss в css*/
gulp.task('sass',function () {
    return gulp.src(dir_scss) /* *.+(scss|sass) */
        .pipe(sass())
        .pipe(autoprefixer(['last 15 versions','> 1%','ie 8','ie 7'],{
            cascade:true
        }))
        .pipe(gulp.dest('app/css'))
        .pipe(concat("styles_project.css"))
        .pipe(gulp.dest(dir_web_prod+'css'))
        .pipe(browserSync.reload({
            stream:true
        }));
});

    gulp.task('js',function () {
        return gulp.src(dir_js)
            .pipe(gulp.dest(dir_web_prod+'js'))
            .pipe(browserSync.reload({
                stream:true
            }));
    });

    /*Грузим js библиотеке для подгрузки*/
gulp.task("scripts",['js'],function () {
    return gulp.src([
        dir_dev_lib+'/jquery/dist/jquery.min.js'
        ])
            .pipe(concat('libs.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('app/js'))
            .pipe(gulp.dest(dir_web_prod+'js'));
});

/*Собираем шрифты в общую папку*/
gulp.task('fonts_dest',function () {
    return gulp.src(dir_fonts)
        .pipe(gulp.dest(dir_web_prod+'fonts'));
});

/*Браузер синх*/
gulp.task('browser-sync',function () {
    browserSync({
        proxy:"tisbi-test.web:8080"
    });
});

/*Работа с картинками*/
gulp.task('img',function () {
    return gulp.src(dir_img)
        .pipe(cache(imagemin({
            interlaced:true,
            progressive:true,
            svgoPlugins:[{
                removeViewBox:false
            }],
            use:[pngquant()]
        })))
        .pipe(gulp.dest(dir_web_prod+'img'));
});

gulp.task('clean_cash',function () {
    return cache.clearAll();
});

/*Слежение файлов*/
gulp.task('watch',['browser-sync','sass','img','fonts_dest','scripts'],function () {
    gulp.watch(dir_js,['js']);
    gulp.watch(dir_scss,['sass']);
    gulp.watch(dir_img,['img']);
    gulp.watch(dir_web_prod+"/**/*.php").on("change",browserSync.reload);
    gulp.watch(dir_view+"/**/*.tpl").on("change",browserSync.reload);
});

gulp.task("build",['clean','sass','scripts','img'],function () {

    var build_css = gulp.src([
        dir_css,
        dir_min_css
    ])
        .pipe(gulp.dest('dist/css'));

    var build_fonts = gulp.src([
        'app/fonts/**/*'
    ])
        .pipe(gulp.dest('dist/fonts'));

    var build_js = gulp.src([
        dir_js,
        dir_min_js
    ])
        .pipe(gulp.dest('dist/js'));

    var build_html = gulp.src([
        dir_html
    ])
        .pipe(gulp.dest('dist'));


});