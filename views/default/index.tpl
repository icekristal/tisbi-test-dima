<form action="javascript:void(null);" id="testing_form" class="b-field-test" onsubmit="SendTestingForm(this);">
    <div class="b-field-test__row">
        <label for="field_one">Поле 1</label>
        <input type="text" id="field_one" name="field_one" required>
    </div>
    <div class="b-field-test__row">
        <label for="field_two">Поле 2</label>
        <input type="text" id="field_two" name="field_two" pattern="\d+(,[ 0-9]+$)?" required>
    </div>
    <div class="b-field-test__row">
        <label for="field_three">Поле 3</label>
        <textarea rows="5" id="field_three" name="field_three" required></textarea>
    </div>
    <div class="b-field-test__row">
        <input type="submit" value="Отправить">
        <input type="reset" value="Отчистить">
    </div>

    <div class="b-field-test__message">

    </div>
</form>






<div class="b-field-test">
    {if isset($array_setting)}
        {foreach $array_setting as $set}
            {$set}
            <br>
        {/foreach}
    {/if}
</div>