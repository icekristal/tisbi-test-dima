<?php
/**
 * Created by PhpStorm.
 * User: Savatneev Anton Alex
 * Date: 25.01.2018
 * Time: 12:20
 */

namespace components;


class Settings
{

    /*Я не совсем полнял смысл 2 части, раньше не использовал в таком виде настройки проекта, разве что подключение к БД...*/
    protected static $_instance;

    private $settings_project = array();

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    private function __construct() {
    }


    /**
     * @return array
     */
    public function getSettingsProject($param=null)
    {
        if(isset($param)){
            return $this->checkingKeyForSettting($param) ? $this->settings_project[$param] : null;
        }
        return $this->settings_project;
    }

    /**
     * Добавление массива настроек
     * @param array $settings_project
     */
    public function setSettingsProject($settings_project)
    {
        $this->settings_project = $settings_project;
    }

    public function setNewSetting($key,$value){
        if($this->checkingKeyForSettting($key)){
            return false;
        }
        return $this->settings_project[$key] = $value;
    }

    /**
     * @param $param
     */
    public function delSettingProject($param){
        if($this->checkingKeyForSettting($param)){
            unset($this->settings_project[$param]);
            return true;
        }
        return false;
    }

    /**
     * @param $param
     */
    public function updateSettingProject($param,$value){
        if($this->checkingKeyForSettting($param)){
            $this->settings_project[$param]=$value;
            return true;
        }
        return false;
    }

    private function checkingKeyForSettting($key){
        $check = array_key_exists($key,$this->settings_project);
        return $check;
    }


    private function __clone() {
    }
    private function __sleep() {
    }
    private function __wakeup() {
    }
}