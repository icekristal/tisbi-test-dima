<?php

namespace components;

use Smarty;


class SettingsSmart extends Smarty
{

    public $file_name = "";
    private $last_prefix = TemplatePostfix;
    public function __construct(){

        parent::__construct();

        $this->setTemplateDir(TemplatePrefix);
        $this->setCompileDir(S_ROOT.'/tmp/smarty/template_c');
        $this->setCacheDir(S_ROOT.'/tmp/smarty/cache');
        $this->setConfigDir(S_ROOT.'/vendor/Smarty/configs');

        $this->assign('TemplateWebPath',TemplateWebPath);
    }

    /**
     * @param string $file_name
     */
    public function LoadSmarty($file_name)
    {
      return $this->display($file_name.$this->last_prefix);
    }
}